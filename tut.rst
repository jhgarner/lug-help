Links to LiveCD ISOs:
=====================

You will need to get the proper ISO for your processor architecture, which
is probably 64-bit x86 unless your computer is unusual or very old.

* | Debian: https://lug.mines.edu/mirrors/debian-cdimage/
  | (Use the unofficial ISO with non-free software included if you have hardware that needs proprietary drivers, like a Broadcom WiFi card: https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/)
  
* Ubuntu: https://lug.mines.edu/mirrors/ubuntu-releases/

* Fedora: https://lug.mines.edu/mirrors/fedora-cdimage/

* Arch: https://lug.mines.edu/mirrors/archlinux/iso/

Are there risks to installing Linux?
====================================
The biggest risk when installing Linux is the loss of data. Everything else
is recoverable and at worse will just take some time to fix. With that said,
even data loss is extremely unlikely as installers for distributions like Ubuntu
are designed to keep previously installed operating systems untouched. Although we've
never seen anyone lose data, it is worth backing up any extremely important
files you couldn't risk losing.

What does installing Linux mean?
================================

Linux can be installed either on real hardware or on virtual hardware, both with
pros and cons.

If you're currently using Windows, you have a few options available to you.

#. Dual boot Linux. Every time you reboot your computer, you can pick whether
   you want to boot Linux or Windows. If you want to try out Linux on real
   hardware to get a feel for it, this is probably what you want to do.
   
   * Pro: Gets the full power of Linux on your computer

   * Pro: Since only one OS is booted at a time, resource usage isn't any higher.

   * Pro: You always have an escape hatch back to Windows if you need it.

   * Con: Switching between OS's requires a full reboot

#. Install Linux in a VM. A virtual machine can run on Windows and pretend to be
   a computer inside a computer. You can then install Linux on this fake
   computer. Typically, This means you will have a window in Windows which shows
   what would be on the screen of a computer running Linux.

   * Pro: No compatibility problems as the fake computer pretends to use hardware supported by Linux

   * Pro: Doesn't touch how your computer actually boots.

   * Con: Requires a lot of computing resources. Before setting up a VM, your computer only needed enough power to run Window or Linux. While running a VM, your computer needs enough resources to run both. This is most noticeable with RAM usage and GPU power.

#. Use Windows subsystem for Linux. The most recent versions of Windows include
   support for emulating the Linux kernel allowing you to run Linux programs
   inside of Windows.

   * Pro: Access to powerful CLI tools without leaving Windows

   * Pro: Much less resource intensive than a VM

   * Con: Setting up GUI tools is nontrivial.

   * Con: You lose out on many of the customizations Linux normally offers particularly with respect to the GUI.

#. Wipe Windows and just install Linux.

   * Pro: Gets the full power of Linux on your computer with all of the disk space.

   * Con: Moving back to Windows requires a reinstall which will make booting back into Linux somewhat annoying.

   * Con: Any data not backed up is lost.

Which should you pick?
----------------------
For most people, option 1 is your best bet. It captures all of the benefits of
Linux without having to abandon your previous set up.

What Distro should I install?
-----------------------------
The distro you pick determines your defaults and the kinds of updates you will
receive. If you've never used Linux before, you probably want something with
strong defaults to make the install process easier. The next question is which
kind of updates do you want. If you prefer having the latest and greatest
software at the expense of support, your best bet is probably Fedora. If you prefer having well supported
but older software, your best bet is probably Ubuntu. It's worth noting that
these two options aren't polar opposites. Instead, you can think of them
as small variations on each other where the tradeoffs are fairly minor.

If you're still not sure, Ubuntu is probably your best bet.

If you have a specific distro in mind, let someone know and we can help you with
that.

How do I install Linux?
=======================
the following assumes you picked option 1 from up above.

Prepping the USB
----------------
Almost all computers support booting from a USB. If you need to make a bootable
usb, the Windows program Rufus is recommended for writing your distro's iso file
to a USB and making it bootable. It is recommended that you select dd mode when
prompted as that has the least number of compatibility problems.

You can download your distro's iso from their own website or from one of the
mirrors at Mines. The mirrors should download much faster.

Prepping Windows
----------------
Preparing Windows for the other operating system is the next step. If you
right click the start button, you should see an option called disk management.
The disk management program allows you to resize your partitions.

.. image:: https://www.lifewire.com/thmb/zR6YVvIDebsSJur5tO7MhxhSxaU=/768x0/filters:no_upscale():max_bytes(150000):strip_icc()/disk-management-windows-10-58a5d33a3df78c345b052f96.PNG

Your hard drive is generally treated as a long line of data which is then
segmented into pieces. By default, Windows segments it into one big piece (the C
drive) and a couple of smaller pieces for recovery purposes. Windows supports
safely shrinking pieces to make free space. Once disk manager loads, you want to
right click the partition you want to shrink. This is most likely the C drive.
Once there is free space, the Linux installer will be able to find it and create
it's own partition there.

Do you want to be able to write to your Windows files from Linux? If so, you
need to disable fastboot. This can be found under the power settings in the
control panel.

Finally, we need to reboot into your UEFI firmware settings. These allow you to
change various settings related to how your computer boots. The easiest way to
boot into it is to open the settings up, click "Update and Security", click
recovery, then "restart now" under the advanced startup section. Windows will
then ask you to select "Advanced Options" and "UEFI Firmware Settings".

What we do now depends on a couple of factors. No matter what though, we want to
plug in the USB and change the boot order such that the USB comes first. The big
question now is whether we want to disable secure boot as well. Secure boot is a
system to prevent malicious code from affecting how your computer boots. It
requires your OS be signed by a trusted party. Most computers only trust
Microsoft as that party. Luckily, several disros like Ubuntu and Fedora
have worked with Microsoft to get their software signed through them. Depending on your computer, you
might be able to leave secure boot on. If you have a Dell computer and Ubuntu,
the installer will actually insist you turn it on. For that reason, you probably
want to leave it on unless you run into problems.

Following the Installer
-----------------------
At this point, whatever distro you've chosen should walk you through the process
of installing Linux. In general, you want to make sure to say yes to any
questions about third party software. Once installed, what you do next depends
on the distro and is mostly up to you.

.. image:: https://www.tecmint.com/wp-content/uploads/2015/04/Ubuntu-Software-Updates.png

Special hardware
================

Some hardware can make running Linux harder.

Nvidia Optimus Laptops
----------------------

If you have both a Nvidia card and an Intel card, your laptop is probably
designed to switch between the two on the fly. The Nvidia card uses more power
but has better performance while the Intel card is the opposite. Official
support for this kind of setup is currently in beta, but there are still ways to
make it work today. Until official support arrives, you should expect the
switching process to be less convenient. Ubuntu comes with some form of support,
but switching cards requires logging out and back into your computer. A project
called bumblebee gets around logging in and out but only supports using Opengl,
not Vulkan. Alternatively, you can choose to just run one of the two cards
exclusively but that negates many of the benefits you get by having two.

Using the Nvidia driver may require you disable secure boot particularly on
Fedora. 

Broadcom WiFi
-------------

If you have a WiFi card made by Broadcom, it won't work without installing 
some special drivers. This can be quite the catch-22. Luckily, you can use one 
of the Ethernet cables in the room or boot into one of the installation medias 
which support your card. Most laptops use Intel's WiFi cards which work fine.

If you're installing Debian, it's easiest to use the non-free install ISO.
